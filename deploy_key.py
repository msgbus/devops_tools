#!/usr/bin/env python
#coding:utf-8

import sys
import getopt

import requests

baseurl = 'https://api.bitbucket.org/1.0/repositories/msgbus/'
Account = None
Password = None
Repos_dir = None
Operation = None
ssh_keys_dir = None
Repos = None
Label = None

def parse_opt():
    global Account, Password, Repos_dir, Operation,ssh_keys_dir,Repos,Label
    opts,args = getopt.getopt(sys.argv[1:],':p:r:o:f:a:s:l:')
    for op,value in opts:
        if op == '-p':
            Password = value
        elif op == '-r':
            Repos_dir = value
        elif op == '-o':
            Operation = value
        elif op == '-a':
            Account = value
        elif op == '-f':
            ssh_keys_dir = value
        elif op == '-s':
            Repos = value
        elif op == '-l':
            Label = value
        else:
            print 'parse error'

def parse_ssh_keys(ssh_keys_dir):
    label = []
    ssh_keys = []
    with open(ssh_keys_dir) as file:
        for line in file:
            data = line.split(':')
            label.append(data[0].strip())
            ssh_keys.append(data[1].strip())
    return zip(label,ssh_keys)

def parse_repos_list(repos_list_dir):
    repos = []
    with open(repos_list_dir) as file:
        for line in file:
            data = line.split(',')
            for temp in data:
                repos.append(temp)
    return repos

def result_info(statuscode):
    if statuscode == 400:
        print 'error:400,bad request'
    elif statuscode == 401:
        print 'Error:401,Unauthorized'
    elif statuscode == 403:
        print 'Error:403,Forbidden'
    elif statuscode == 404:
        print 'Error:404,Not found'
    elif statuscode == 405:
        print 'Error:405,Method not allowed'
    elif statuscode == 409:
        print 'Error:409,Conflict'
    elif statuscode == 415:
        print 'Error:415,Unsupported media type'
    else:
        print 'error:'+statuscode

class Deploy_Keys(object):
    def __init__(self):
        self.baseurl = baseurl
        self.Password = Password
        self.Account = Account
        self.Repos = Repos
        self.session = requests.session()

    def  add_keys(self):
        result = []
        ssh_keys_list = parse_ssh_keys(ssh_keys_dir)
        for sel_Repos in parse_repos_list(Repos_dir):
            url = baseurl+sel_Repos+'/deploy-keys'
            for label,ssh_data in ssh_keys_list:
                result.append(self.session.post(url,data={'key':ssh_data,'label':label},auth=(self.Account,self.Password)))
        return result

    def delete_keys(self,key_number):
        url = baseurl+Repos+'/deploy-keys/'+str(key_number)
        result = self.session.delete(url,auth=(self.Account,self.Password))
        return result

    def get_keys_list(self):
        url = baseurl+str(Repos)+'/deploy-keys'
        result = self.session.get(url,auth=(self.Account,self.Password))
        return result

    def get_host_keys(self):
        key_list = self.get_keys_list()
        for item in key_list.json():
            if item['label'] == Label:
                return item['pk']
        return None

if __name__ == '__main__':
    parse_opt()
    tempobject = Deploy_Keys()
    statuscode = None
    if Operation == 'del':
        pk = tempobject.get_host_keys()
        if pk:
            result = tempobject.delete_keys(pk)
            statuscode = result.status_code
            if statuscode == 204:
                print 'delete success'
            else:
                result_info(statuscode)
        else:
            print 'no such ssh_keys,can\'t delete'

    elif Operation == 'add':
        result_all = tempobject.add_keys()
        for result in result_all:
            statuscode = result.status_code
            if statuscode == 200:
                print result.content
            else:
                result_info(statuscode)

    elif Operation == 'list':
        result = tempobject.get_keys_list()
        statuscode = result.status_code
        if statuscode == 200:
            if result.content:
                for item in result.json():
                    print 'pk:'+str(item['pk'])+'-----label:'+item['label']
                    print 'key:'+item['key']
                    print '---------------------------'
        else:
            result_info(statuscode)

    elif len(sys.argv) == 1:
        print 'help documention \n' \
              'how to use this python scripts:\n' \
              'please install requests:pip install requests\n' \
              'then use this command:\n' \
              '1 list ssh_keys:\n' \
              './deploy_key.py -a account -p password -s select_a_repos -o list\n' \
              '2 add ssh_keys:\n' \
              './deploy_key.py -a account -p password -r -o add /path/to/repos/list -f /path/to/ssh_keys/list/file \n' \
              '3 del ssh_keys:\n' \
              './deploy_key.py -a account -p password -s select_a_repos -o del -l label_for_ssh_keys_you_want_to_delete'

    else:
        print 'operation error'
