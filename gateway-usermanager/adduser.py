#!/usr/bin/env python

# author: gongbibo@yunba.io

from __future__ import print_function
import sys
import os
import subprocess
import json

def read_json(json_file):
	with open(json_file) as f:
		user_dict = json.loads(f.read())

	# print('servers:', user_dict['servers'])
	return user_dict


def adduser(username):
	subprocess.call('useradd -m {} -p "" --shell "/bin/bash"'.format(username), shell=True)


def ssh_keygen(username):
	subprocess.call('su {0} -c "ssh-keygen -t rsa -C \\\"{0}@yunba.io\\\" -f /home/{0}/.ssh/id_rsa -P \\\"\\\""'.format(username), shell=True)


def add_ssh_key(username, ssh_key):
	if os.path.isdir('/home/{}/.ssh'.format(username)):
		pass
	else:
		subprocess.call('su {} -c "mkdir ~/.ssh"'.format(username), shell=True)

	subprocess.call('su {} -c "echo {} >> ~/.ssh/authorized_keys"'.format(username, ssh_key), shell=True)


def main():
	json_file = sys.argv[1]
	user_dict = read_json(json_file)
	username, ssh_key = user_dict['username'], user_dict['ssh_key']
	print('username:', user_dict['username'])
	print('ssh key:', user_dict['ssh_key'])

	if not username:
		return

	else:
		adduser(username)
		if ssh_key:
			add_ssh_key(username, ssh_key)
		else:
			pass
		ssh_keygen(username)


if __name__ == '__main__':
	main()
