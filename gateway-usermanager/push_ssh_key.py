#!/usr/bin/env python

# author: gongbibo@yunba.io

from __future__ import print_function
import sys
# import os
import subprocess
import json
import adduser

def make_hosts(server_list):
	with open('hosts','w') as f:
		[ f.write(server + '\n') for server in server_list ]


def push_ssh_key(username, server_username):
	# print('ansible all -i hosts -u ' + server_username + ' -m authorized_key -a \"user=' + server_username + ' key={{lookup(\'file\',\'/home/test/.ssh/id_rsa.pub\')}}"')
	subprocess.call('ansible all -i hosts -u ' + server_username + ' -m authorized_key -a \"user=' + server_username + ' key={{lookup(\'file\',\'/home/' + username + '/.ssh/id_rsa.pub\')}}"', shell=True)


def main():
	json_file = sys.argv[1]
	user_dict = adduser.read_json(json_file)
	username, server_list, server_username = user_dict['username'], user_dict['servers'], user_dict['server_username']
	print(server_list)
	make_hosts(server_list)
	push_ssh_key(username, server_username)


if __name__ == '__main__':
	main()
