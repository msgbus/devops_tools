#gateway-useradd


在 gateway 运行，添加 新用户 + ssh key，并创建新密钥对，push 公钥到 server.

---

###Depends
* Python2 / Python3
* Ansible

###Usage
1 clone 项目
```
git clone git@bitbucket.org:msgbus/devops_tools.git
cd devops_tools/gateway-usermanager
```

2 编辑 user.json，参考 example.json

>* username：要在 gateway 创建的用户名（比如新员工 tiger)
* ssh_key：tigher的办公 pc 的ssh key
* server_username：能够登录的 server 上的 user(比如 yunba)
* servers：该用户可以登录的 server

3 运行
```
# 添加新用户，写 ssh key 到 authorized_keys，并创建新密钥对，push 公钥到 server：
sh ./adduser_pushkey.sh user.json

# 仅添加新用户，写 ssh key 到 authorized_keys，并创建新密钥对：
sudo python adduser.py user.json

# 仅将新用户的 ssh key push 到 server:
python push_ssh_key.py user.json
```

###P.S.
ssh 日志位置：
```
/var/log/auth.log
```
