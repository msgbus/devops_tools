from flask import Flask, session, request, redirect, url_for, abort
# import json
import subprocess
import socket
import hashlib
from gevent.wsgi import WSGIServer
# from gevent import monkey
# monkey.patch_socket()

app = Flask(__name__)
app.secret_key = b'\xa7\x89\x01\xca\x87tv\xd6\xdc\x8eHF\xa3\x08\xc3\x16\t\x98\xd0>\xa3\xc7]\x14'

def is_inside(ip):
	white_list = subprocess.getoutput('iptables-save')
	return ip in white_list


def add_white_ip(ip):
	# ssh_port = json.loads(open('addwhiteip.json').read())['ssh_port']
	if is_inside(ip):
		return
	else:
		command = 'iptables -I INPUT -s {ip} -p tcp --dport {port} -j ACCEPT'.format(ip=ip, port=22)
		subprocess.call(command, shell=True)
		subprocess.call('/etc/init.d/iptables-persistent save', shell=True)
	

def check(ip):
	try:
		socket.inet_aton(ip)
		return True
	except:
		return False


def deal_with_ip(ip):
	if check(ip):
		add_white_ip(ip)
		return 'ok.'
	else:
		return 'error'


def deal_with_password(password):
	m = hashlib.md5((password + 'abnuy').encode())
	with open('passwd.dat', 'w') as f:
		f.write(m.hexdigest())
	return 'ok.'


def get_password():
	with open('passwd.dat') as f:
		return f.read().strip()


def hash(password):
	m = hashlib.md5((password + 'abnuy').encode())
	return m.hexdigest()


@app.route('/addip', methods=['GET', 'POST'])
def addip():
	if not session.get('key'):
		abort(403)
	elif not hash(session['key']) == get_password():
		# return 'error'
		return redirect( url_for('index') )

	else:
		if request.method == 'POST':
			if request.form.get('ip'):
				ip = request.form['ip']
				return deal_with_ip(ip)
			elif request.form.get('password'):
				password = request.form['password']
				return deal_with_password(password)

		else:
			# print(request.remote_addr)
			return '''
		        <form action="" method="post">
			当前ip:''' + request.remote_addr + '''
			</br>
		            ip:<p><input type=text name=ip>
		            <p><input type=submit value=add>
			</br>
		            new password:<p><input type=password name=password>
		            <p><input type=submit value=change>
		        </form>
		    '''


@app.route('/', methods=['GET', 'POST'])
def index():
	if request.method == 'POST':
		session['key'] = request.form['key']
		return redirect( url_for('addip') )
	else:
		return '''
		        <form action="" method="post">
		            password:<p><input type=password name=key>
		            <p><input type=submit value=Login>
		        </form>
		    '''


if __name__ == '__main__':
	# add_white_ip('192.168.2.101')
	WSGIServer(('', 5000), app, keyfile='yunba.pem',certfile='yunba.pem').serve_forever()
	# app.run(debug=True, ssl_context='adhoc')
