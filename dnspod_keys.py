#!/usr/bin/env python
#!coding:utf-8

import requests
import getopt
import sys

Domain = None
IP = None
Operation = None
Type = 'A'
Sub_Domain = 'www'
TokenID = None #'16432'#None
Token = None #'8b632c33a870088df411f475a83e1860'#None
Record_id = None

def parse_opt():
    global Domain,IP,Operation,Type,Sub_Domain,TokenID,Token,Record_id
    opts,args = getopt.getopt(sys.argv[1:],':d:a:o:t:s:i:n:')
    for op,value in opts:
        if op == '-a':
            IP = value
        elif op == '-d':
            Domain = value
        elif op == '-o':
             Operation = value
        elif op == '-t':
            Token = value
        elif op == '-i':
            TokenID = value
        elif op == '-n':
            Record_id = value
        else:
            print 'parse error'

class Dnspod(object):
    def __init__(self):
        self.session = requests.session()
    def add_a_record(self):
        url = 'https://dnsapi.cn/Record.Create'
        postdata = {'login_token':str(TokenID)+','+Token,'format':'json','domain':Domain,'sub_domain':Sub_Domain,'record_type':Type,'record_line':u'默认','value':IP}
        response = self.session.post(url,data = postdata)
        result = response.json()
        status = result["status"]['code']
        if status == '1':
            print 'Add success \n' \
                  'new record id:'+result['record']['id']+' type:'+result['record']['name']+' status:'+result['record']['status']
        else:
            print 'Error:' + result['status']['message']

    def list_record(self):
        url = 'https://dnsapi.cn/Record.List'
        postdata = {'login_token':str(TokenID)+','+Token,'format':'json','domain':Domain}
        response = self.session.post(url,data = postdata)
        result = response.json()
        status = result["status"]['code']
        if status == '1':
            print 'domain id:'+result['domain']['id']+' domain name:'+result['domain']['name']
            print 'records:'
            for record in result['records']:
                print 'record id:'+record['id']+' status:'+record['status']+' type:'+record['name']+' value:'+record['value']
        else:
            print 'Error:' + result['status']['message']

    def del_record(self):
        url = 'https://dnsapi.cn/Record.Remove'
        postdata = {'login_token': str(TokenID) + ',' + Token, 'format': 'json', 'domain': Domain,
                    'record_id':Record_id}
        response = self.session.post(url, data=postdata)
        result = response.json()
        status = result["status"]['code']
        if status == '1':
            print 'Delete success'
        else:
            print 'Error:' + result['status']['message']

if __name__ == '__main__':
    parse_opt()
    dns = Dnspod()
    if Operation == 'list':
        dns.list_record()
    elif Operation == 'add':
        dns.add_a_record()
    elif Operation == 'del':
        dns.del_record()
    elif len(sys.argv) == 1:
        print '           help documention\n' \
              'first please install requests:pip install requests\n' \
              'How to use this python scripts:\n' \
              '1 add a A record:\n' \
              './dnspod_key.py -a {ip} -d {domain like:python.org} -o add -t {token} -i {token id}\n' \
              '2 list all record of a domain:\n' \
              './dnspod_key.py -d {domain} -o list -t {token} -i {token id}\n' \
              '3 delete a record of a domain:\n' \
              './dnspod_keys.py -d {domain} -o del -t {token} -i {token id} -n {record_id}'
    else:
        print 'operation error'
