# mongodb升级-(2.6-3.2)

## 官方建议升级路线

	2.6-3.0-3.2

## 2.6-3.0升级兼容性

### 存储引擎

- 配置文件选项更改：配置文件和已经存在的配置文件向后兼容，但是在使用旧的设置时会引发warning

- 数据文件必须对应配置的存储引擎：当创建数据文件的存储引擎和配置指定的存储引擎不同时mongod会无法启动

- 支持WiredTiger存储引擎和驱动版本兼容性：2.6.8版本的mongo shell和使用wiredtiger存储引擎的mongod实例兼容，但是以下命令无输出：

		db.getCollectionNames()
		db.collection.getIndexes()
		show collections
		show tables

- `db.fsyncLock()`调用和WiredTiger存储引擎不兼容：调用上述函数时不保证数据文件被改变

- WiredTiger存储引擎不支持`touch`命令，执行时会返回错误，MMAPV1存储引擎支持`touch`命令

- 动态的`record`分配：3.0版本不再支持动态的动态的`record`分配，同时废弃了`paddingFactor`

###复制变化

- 复制集oplog格式改变：3.0版本不兼容mongodb2.2.1版本以前生成的oplog条目

- 复制集配置验证：mongodb提供一个严格复制集配置设置验证，包括：决定者只有一票；非决定者只可以有其他成员投票权的0票或者一票；复制集配置文件中的`_id`的名字必须和`--replset`选项或者`replication.replSetName`相同；不允许选项`settings.getLastErrorDefaults`的值为0；设置中只能包含可以识别的选项。**必须要在升级前修改配置文件**

- `w: majority`语义的改变：当大部分的投票成员复制了这个写操作时一次包含`w: majority`值得写入才会被满足

- 删除了`local.slaves`集合：mongodb3.0删除了跟踪从节点复制操作的`local.slaves`集，想要跟踪复制进程，使用`rs.status()`方法

- 复制集状态改变：`FATAL1`复制集状态不在存在

- HTTP接口：http接口不在汇报复制数据

###mongodb工具变化

- 需要一个运行的mongodb实例：3.0版本的mongodb工具：` mongodump, mongorestore, mongoexport, mongoimport, mongofiles, and mongooplog`必须连接一个运行的mongodb实例，不能直接通过`--dbpath`选项修改数据文件。

- 删除了一些选项

	- mongodump, mongorestore, mongoimport, mongoexport, and bsondump:`--dbpath, --journal, and --filter`
	
	- mongotop:`--locks`

	- bsondump,mongorestore:`--noobjcheck`

	- mongoexport:`--csv`,使用`--type`选项来指明格式类型

### 分片集群设置

- 删除了`releaseConnectionsAfterResponse`参数：mongodb现在总是在响应后就释放连接

### 安全性变化

- 删除2.4版本中的用户模型：

	if your authSchema version is less than 3 or the query does not return any results, see Upgrade User Authorization Data to 2.6 Format to upgrade the authSchema version before upgrading to MongoDB 3.0.

	After upgrading MongoDB to 3.0 from 2.6, to use the new SCRAM-SHA-1 challenge-response mechanism if you have existing user data, you will need to upgrade the authentication schema a second time. This upgrades the MONGODB-CR user model to SCRAM-SHA-1 user model. See Upgrade to SCRAM-SHA-1 for details.

- 本地异常改变

- 删除了`db.addUser()`调用：3.0版本删除了传统的`db.addUser`调用，使用`db.createUser`和`db.updateUser`替代

- TLS/SSL配置选项变化：3.0版本在配置文件中使用新的` net.ssl.allowConnectionsWithoutCertificates`配置，在mongod和mongos使用新的`--sslAllowConnectionsWithoutCertificates`命令行选项代替了原来的`net.ssl.weakCertificateValidation`和` --sslWeakCertificateValidation`选项

- TLS/SSL证书有效性验证：默认当运行SSL模式时，mongodb实例只有在证书验证有效时才会启动

- TLS/SSL证书主机名有效性验证：mongodb默认会通过尝试使用证书连接证书中的hostnames验证hosts中的hostname

- 禁用SSLv3密码

- mongo shell兼容性：3.0版本以前的mongo shell不兼容需要强制访问控制的3.0部署版本mongodb

- HTTP状态接口和REST API兼容性：3.0版本中推行HTTP状态接口和REST API支持` SCRAM-SHA-1 `质询-响应用户验证机制

### 索引

- 删除`dropDups`选项：`dropDups`选项不再对`createIndex(),ensureIndex(),createIndexs`调用可用

- 进行后台建立索引失败时将操作更改到重启操作

- 当执行使用2d index的`$nearSphere`查询时，mongodb不在使用默认的100文档的限制

### 驱动兼容性变化

- Each officially supported driver has release a version that includes support for all new features introduced in MongoDB 3.0. Upgrading to one of these version is strongly recommended as part of the upgrade process.

### 常规兼容性变化

- `findAndModify`返回文档：

- upsert:true with a Dotted _id Query

- 废弃对`system.indexes`和`system.namespaces`的访问：3.0版本中使用`createIndexes`和`listIndexes`代替

- Collection名字有效性检测：3.0版本更加坚定执行collection naming restrictions

- 删除或者废弃不用的命令和方法

		closeAllDatabases
		getoptime
		text
		indexStats, db.collection.getIndexStats(), and db.collection.indexStats()
		diagLogging
		eval, db.eval()
		db.collection.copyTo()
	此外，不能使用已经废弃的`eval`命令或者`db.eval`命令去调用`mapReduce`或者`db.collection.mapReduce()`

- `Date`和`Timestamp`比较排序：3.0版本不在将Timestamp和Date数据类型看成相等，Timestamp数据类型比Date数据类型有更高的比较/排序等级

- Server Status输出变化：`serverStatus`和`db.serverStatus()`方法不在返回`indexCounters`和`recordStats`字段

- Unix Socket权限变化：Unix domain socket文件的权限现在默认是0700，修改这个权限，mongodb提供`net.unixDomainSocket.filePermissions`设置和`--filePermission`选项

- clone Collection:`cloneCollection`命令和`db.cloneCollection()`方法现在返回错误而不是插入当这个collection存在的时候




