# mongodb升级-(2.6-3.2)

## 官方建议升级路线

	2.6-3.0-3.2

## 3.0-3.2升级兼容性

### 默认存储引擎变更

- mongodb使用WiredTiger作为默认的存储引擎，对于已经存在的部署，如果没有指定`--storageEngine`或者`storage.engine`选项，mongodb将会自动决定使用创建数据文件时使用的存储引擎

### 索引变更

- Version 0索引：3.2版本不允许创建version 0索引，如果version 0 索引存在，mongodb将会输出一个警告日志，注明了collection和index

### 聚合函数兼容性变更

- $avg累加器返回null当遇到一个不存在的field时，先前的版本返回0

- $substr返回errors当结果不是一个无效的UTF-8，先前的版本打印出无效的UTF-8结果

- 数组元素在聚合管道中不再被当成文字，相反的，每个元素都被当做表达式处理，可以使用$literal操作符将元素转换成文字

### SpiderMoney兼容性变更

- 3.2版本将JavaScripts引擎从V8更换为SpiderMonkey

### 驱动兼容性变更

- 驱动升级是必要的为了查找获得更多的命令

### 常规兼容性变更

- 3.2版本丢弃了`cursor.showDiskLoc()`方法为了支持`cursor.showRecordId()`方法，都返回一个新的文档格式

- 3.2版本将`serverStatus.repl.slaves`field重命名为`repl.replicationProgress`

- 默认选项`--moveParanoia`变更为`--noMoveParanoia`

- 3.2版本中有1票的复制集成员不能从有0票的成员同步数据

- mongooplog从3.2版本开始废弃

