#!/bin/bash  

INFLUXDB_HOST="{{influxdb_common_stat}}"
INFLUXDB_PORT={{influxdb_common_stat_port}}
SampleTime=1    # seconds

ProcStat=/proc/stat

TotalCpuTime1=`cat $ProcStat | grep 'cpu ' | awk '{ print $2 + $3 + $4 +$5 + $6 + $7 + $8 + $9 }'`
IdleTime1=`cat $ProcStat | grep 'cpu ' | awk '{ print $5 }'`
sleep ${SampleTime}
TotalCpuTime2=`cat $ProcStat | grep 'cpu ' | awk '{ print $2 + $3 + $4 +$5 + $6 + $7 + $8 + $9 }'`
IdleTime2=`cat $ProcStat | grep 'cpu ' | awk '{ print $5 }'`

debug() {
    echo "TotalCpuTime1 = $TotalCpuTime1"
    echo "IdleTime1 = $IdleTime1"
    echo "TotalCpuTime2 = $TotalCpuTime2"
    echo "IdleTime2 = $IdleTime2"
    echo "CPU_USAGE = $CPU_USAGE"
}

CPU_USAGE=`echo "($TotalCpuTime2 - $TotalCpuTime1 - $IdleTime2 + $IdleTime1)/($TotalCpuTime2 - ${TotalCpuTime1})*100" | bc -l`

#debug

echo "cpu_usage_stat,host={{inventory_hostname}} value=${CPU_USAGE}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
