#!/bin/bash  

INFLUXDB_HOST="{{influxdb_common_stat}}"
INFLUXDB_PORT={{influxdb_common_stat_port}}
SampleTime=3    # seconds

ProcEth0StatPath=/sys/class/net/eth0/statistics
ProcEth1StatPath=/sys/class/net/eth1/statistics

Eth0_RXPre=`cat $ProcEth0StatPath/rx_bytes`
Eth0_TXPre=`cat $ProcEth0StatPath/tx_bytes`
Eth1_RXPre=`cat $ProcEth1StatPath/rx_bytes`
Eth1_TXPre=`cat $ProcEth1StatPath/tx_bytes`
sleep ${SampleTime}
Eth0_RXNext=`cat $ProcEth0StatPath/rx_bytes`
Eth0_TXNext=`cat $ProcEth0StatPath/tx_bytes`
Eth1_RXNext=`cat $ProcEth1StatPath/rx_bytes`
Eth1_TXNext=`cat $ProcEth1StatPath/tx_bytes`

Eth0_CurrentRX=`echo "(${Eth0_RXNext} - ${Eth0_RXPre})/131072/${SampleTime}" | bc -l`
Eth0_CurrentTX=`echo "(${Eth0_TXNext} - ${Eth0_TXPre})/131072/${SampleTime}" | bc -l`
Eth1_CurrentRX=`echo "(${Eth1_RXNext} - ${Eth1_RXPre})/131072/${SampleTime}" | bc -l`
Eth1_CurrentTX=`echo "(${Eth1_TXNext} - ${Eth1_TXPre})/131072/${SampleTime}" | bc -l`

debug() {
    echo "current_eth0_rx_speed = ${Eth0_CurrentRX}"
    echo "current_eth0_tx_speed = ${Eth0_CurrentTX}"
    echo "current_eth1_rx_speed = ${Eth1_CurrentRX}"
    echo "current_eth1_tx_speed = ${Eth1_CurrentTX}"
}

# debug

echo "network_traffic_stat_eth0_rx,host={{inventory_hostname}} value=${Eth0_CurrentRX}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "network_traffic_stat_eth0_tx,host={{inventory_hostname}} value=${Eth0_CurrentTX}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "network_traffic_stat_eth1_rx,host={{inventory_hostname}} value=${Eth1_CurrentRX}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "network_traffic_stat_eth1_tx,host={{inventory_hostname}} value=${Eth1_CurrentTX}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
