#sync time
grep ntpdate /etc/rc.local
if [ $? -ne 0 ]; then
    echo -e  "/usr/sbin/ntpdate  time.nist.gov" >> /etc/rc.local
fi

#100W
sed -i '/bridge/s/^/\#/' /etc/sysctl.conf
grep pam_limits /etc/pam.d/login
if [ $? -ne 0 ]; then
    echo -e "session    required     pam_limits.so" >> /etc/pam.d/login
fi
