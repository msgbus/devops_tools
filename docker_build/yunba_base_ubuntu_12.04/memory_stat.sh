#! /bin/bash

INFLUXDB_HOST="{{influxdb_common_stat}}"
INFLUXDB_PORT={{influxdb_common_stat_port}}

MemTotal=`cat /proc/meminfo | grep ^MemTotal: | awk '{print $2}'`
MemUsed=`free | awk 'NR == 3 {print $3}'`
MemFree=`cat /proc/meminfo | grep ^MemFree: | awk '{print $2}'`
Buffers=`cat /proc/meminfo | grep ^Buffers: | awk '{print $2}'`
Cached=`cat /proc/meminfo | grep ^Cached: | awk '{print $2}'`
Active=`cat /proc/meminfo | grep ^Active: | awk '{print $2}'`
Inactive=`cat /proc/meminfo | grep ^Inactive: | awk '{print $2}'`
Slab=`cat /proc/meminfo | grep ^Slab: | awk '{print $2}'`

debug() {
    echo "MemTotal = ${MemTotal}"
    echo "MemFree = ${MemFree}"
    echo "MemUsed = ${MemUsed}"
    echo "Buffers = ${Buffers}"
    echo "Cached = ${Cached}"
    echo "Active = ${Active}"
    echo "Inactive = ${Inactive}"
    echo "Slab = ${Slab}"
}

#debug

echo "memory_stat_total,host={{inventory_hostname}} value=${MemTotal}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "memory_stat_used,host={{inventory_hostname}} value=${MemUsed}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "memory_stat,host={{inventory_hostname}},type=mem_total value=${MemTotal}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "memory_stat,host={{inventory_hostname}},type=mem_used value=${MemUsed}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "memory_stat,host={{inventory_hostname}},type=mem_free value=${MemFree}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "memory_stat,host={{inventory_hostname}},type=mem_buffer value=${Buffers}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "memory_stat,host={{inventory_hostname}},type=mem_cached value=${Cached}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "memory_stat,host={{inventory_hostname}},type=mem_active value=${Active}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "memory_stat,host={{inventory_hostname}},type=mem_inactive value=${Inactive}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
echo "memory_stat,host={{inventory_hostname}},type=mem_slab value=${Slab}" | socat  -t 0 - UDP:${INFLUXDB_HOST}:${INFLUXDB_PORT}
