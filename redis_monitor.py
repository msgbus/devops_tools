#!/usr/bin/env python

import commands
import argparse
import time
from socket import *

Server_dic = {}
Clients_dic = {}
Memory_dic = {}
Persistence_dic = {}
Stats_dic = {}
Replication_dic = {}
CPU_dic = {}
Keyspace_list = []
hostname = gethostname()
timestamp = str(long(time.time() * 1000000000))

def get_redis_data(port):
    status, result = commands.getstatusoutput('redis-cli -p ' + str(port) + ' INFO')
    if status != 0:
        print 'redis-cli error'
        print result
    else:
        redis_data = result.rstrip('\r').split('#')
    for data in redis_data:
        if data:
            list_data = data.strip().split('\r\n')
            if 'Server' == list_data[0]:
                for redis_temp in list_data[1:]:
                    temp = redis_temp.split(':')
                    Server_dic[temp[0]] = temp[1]
            elif 'Clients' == list_data[0]:
                for redis_temp in list_data[1:]:
                    temp = redis_temp.split(':')
                    Clients_dic[temp[0]] = temp[1]
            elif 'Memory' == list_data[0]:
                for redis_temp in list_data[1:]:
                    temp = redis_temp.split(':')
                    Memory_dic[temp[0]] = temp[1]
            elif 'Persistence' == list_data[0]:
                for redis_temp in list_data[1:]:
                    temp = redis_temp.split(':')
                    Persistence_dic[temp[0]] = temp[1]
            elif 'Stats' == list_data[0]:
                for redis_temp in list_data[1:]:
                    temp = redis_temp.split(':')
                    Stats_dic[temp[0]] = temp[1]
            elif 'Replication' == list_data[0]:
                for redis_temp in list_data[1:]:
                    temp = redis_temp.split(':')
                    Replication_dic[temp[0]] = temp[1]
            elif 'CPU' == list_data[0]:
                for redis_temp in list_data[1:]:
                    temp = redis_temp.split(':')
                    CPU_dic[temp[0]] = temp[1]
            elif 'Keyspace' == list_data[0]:
                if list_data[1:]:
                    Keyspace_list.append(list_data[1:])

def parse_memory_data(raw_data):
    memory_point = 'Memory,host='+hostname\
                   +' used_memory='+raw_data['used_memory']+',used_memory_rss='+raw_data['used_memory_rss']\
                   +',used_memory_lua='+raw_data['used_memory_lua']+',used_memory_peak='+raw_data['used_memory_peak']\
                   +' '+timestamp+'\n'
    return memory_point

def parse_stats_data(raw_data):
    value = ''
    for key in raw_data.keys():
        value += (key +'='+raw_data[key]+',')
    stats_point = 'Stats,host=' + hostname+' '+value.rstrip(',')+' '+timestamp+'\n'
    return stats_point

def parse_cpu_data(raw_data):
    value = ''
    for key in raw_data.keys():
        value += (key+'='+raw_data[key]+',')
    cpu_point = 'CPU,host=' + hostname+' '+value.rstrip(',')+' '+timestamp+'\n'
    return cpu_point

def parse_clients_data(raw_data):
    value = ''
    for key in raw_data.keys():
        value += (key + '=' + raw_data[key]+',')
    clients_point = 'Clients,host=' + hostname+' '+value.rstrip(',')+' '+timestamp+'\n'
    return clients_point

def parse_keyspace_data(raw_data):
    if raw_data:
        keyspace_point = ''
        for data in raw_data[0]:
            dbname = data.split(':')[0]
            value = data.split(':')[1]
            keyspace_point += ('Keyspace,host=' +hostname+',db='+dbname+' '+value+' '+timestamp+'\n')
        return keyspace_point
    else:
        return ''

def write_influxdb(host,port,data):
    address =(host,port)
    sock = socket(AF_INET,SOCK_DGRAM)
    if data:
        sock.sendto(data,address)
    sock.close()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-rport", "--redis_port", help="the redis server port",default=6379)
    parser.add_argument("-ihost", "--influxdb_host",help = "the influxdb server host",default = '127.0.0.1')
    parser.add_argument("-iport", "--influxdb_port", help="the influxdb server host", default=6609)
    args = parser.parse_args()
    get_redis_data(args.redis_port)
    data = parse_clients_data(Clients_dic)+parse_cpu_data(CPU_dic)+parse_keyspace_data(Keyspace_list)+parse_memory_data(Memory_dic)+parse_stats_data(Stats_dic)
    write_influxdb(args.influxdb_host,args.influxdb_port,data)

if __name__ == '__main__':
    main()